package configs

import (
	"ginfast/util"

	"github.com/spf13/viper"
)

var (
	// Conf 配置
	GViper  *viper.Viper
	Public  = util.ProcessDir
	AppName = "ginfast"
)

// Load 初始化配置参数
func Load(filename string) error {
	GViper = viper.New()
	GViper.SetConfigFile(Public.Abs(filename))
	if err := GViper.ReadInConfig(); err != nil {
		return err
	}
	Public.Root(GViper.GetString("serve.public"))
	return nil
}

//go:build !embfs
// +build !embfs

package views

import (
	"ginfast/util"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func Static(r *gin.Engine, root string) {
	dist := util.ProcessDir.Abs("views", "dist")
	r.LoadHTMLGlob(dist + "/*.html")
	r.GET(root, func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	r.StaticFile(root+"/favicon.ico", dist+"/favicon.ico")
	files, _ := os.ReadDir(dist)
	for _, fi := range files {
		if fi.IsDir() {
			r.Static(root+"/"+fi.Name(), dist+"/"+fi.Name())
		}
	}
}

package dto

import (
	"ginfast/api/weixin/model"
	"ginfast/pkg/orm"
)

type Where struct {
	*orm.DbPage
	AppType     string `form:"appType"`  //
	UserName    string `form:"userName"` // 用户名
	Phonenumber string `form:"phone"`    // 手机号
	Type        string `form:"type"`
	RepType     string `form:"repType"`
}

func (p *Where) DbModelWhere(v interface{}) *orm.DbWhere {
	w := p.DbWhere()
	switch v.(type) {
	case *model.User:
		w.Like("AppType", p.AppType)
		w.Like("UserName", p.UserName)
		w.Like("Phonenumber", p.Phonenumber)
	case *model.Material:
		w.Like("Tag", p.Type)
		w.Like("RepType", p.RepType)
	}
	return w.Model(v)
}

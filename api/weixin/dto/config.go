package dto

const (
	OfficialCfgName string = "wxofficial"
	AppCfgName      string = "wxapp"
)

// AppCfg 小程序配置参数
type AppCfg struct {
	AppID     string `json:"appId" gorm:"default:'';comment:设置微信小程序的appid"`
	AppSecret string `json:"appSecret" gorm:"default:'';comment:设置微信小程序的app secret"`
}

// OfficialCfg 公众号配置参数
type OfficialCfg struct {
	AppID     string `json:"appId" gorm:"default:'';comment:设置微信公众号的appid"`
	AppSecret string `json:"appSecret" gorm:"default:'';comment:设置微信公众号的app secret;"`
	Token     string `json:"token" gorm:"default:'';comment:设置微信公众号的token;"`
	AesKey    string `json:"aesKey" gorm:"default:'';comment:设置微信公众号的EncodingAESKey;"`
}

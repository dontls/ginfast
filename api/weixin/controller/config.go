package controller

import (
	"ginfast/api/middleware"
	"ginfast/api/weixin/dto"
	"ginfast/api/weixin/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// officialcfg 公众号配置参数
type officialcfg struct {
	AppID     string `json:"appId" gorm:"default:'';comment:设置微信公众号的appid"`
	AppSecret string `json:"appSecret" gorm:"default:'';comment:设置微信公众号的app secret;"`
	Token     string `json:"token" gorm:"default:'';comment:设置微信公众号的token;"`
	AesKey    string `json:"aesKey" gorm:"default:'';comment:设置微信公众号的EncodingAESKey;"`
}

type Config struct {
}

// AppGetHandler 配置
func (Config) AppGetHandler(c *gin.Context) {
	var (
		data   model.Config
		config dto.AppCfg
	)
	if err := orm.DbFirstBy(data, "name like ?", dto.AppCfgName); err == nil {
		util.StringVal(data.Value, &config)
	}
	ginx.JSON(c).WriteData(&config)
}

// AppAddHandler 添加
func (Config) AppAddHandler(c *gin.Context) {
	var p dto.AppCfg
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	data := &model.Config{}
	data.Name = dto.AppCfgName
	data.Value = util.JString(p)
	data.CreatedBy = middleware.GetUser(c).UserName
	err = orm.DbCreate(&data)
	ctx.WriteError(err)
}

// AppUpdateHandler 更新
func (Config) AppUpdateHandler(c *gin.Context) {
	var p dto.AppCfg
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	data := &model.Config{}
	data.Value = util.JString(p)
	data.CreatedBy = middleware.GetUser(c).UserName
	err = orm.DbUpdateModelBy(data, "Name like ?", dto.AppCfgName)
	ctx.WriteError(err)
}

// OfficialGetHandler 配置
func (Config) OfficialGetHandler(c *gin.Context) {
	var (
		data   model.Config
		config officialcfg
	)
	if err := orm.DbFirstBy(data, "Name like ?", dto.OfficialCfgName); err == nil {
		util.StringVal(data.Value, &config)
	}
	ginx.JSON(c).WriteData(gin.H{"data": config})
}

// OfficialAddHandler 添加
func (Config) OfficialAddHandler(c *gin.Context) {
	var p officialcfg
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	data := &model.Config{}
	data.Name = dto.OfficialCfgName
	data.Value = util.JString(p)
	data.CreatedBy = middleware.GetUser(c).UserName
	err = orm.DbCreate(&data)
	ctx.WriteError(err)
}

// OfficialUpdateHandler 更新
func (Config) OfficialUpdateHandler(c *gin.Context) {
	var p officialcfg
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	data := &model.Config{}
	data.Value = util.JString(p)
	data.CreatedBy = middleware.GetUser(c).UserName
	err = orm.DbUpdateModelBy(data, "Name like ?", dto.OfficialCfgName)
	ctx.WriteError(err)
}

func (o Config) Routers(r *gin.RouterGroup) {
	r.GET("/app", o.AppGetHandler)
	r.POST("/app", o.AppAddHandler)
	r.PUT("/app", o.AppUpdateHandler)

	r.GET("/official", o.OfficialGetHandler)
	r.POST("/official", o.OfficialAddHandler)
	r.PUT("/official", o.OfficialUpdateHandler)
}

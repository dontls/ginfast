package controller

import (
	"ginfast/api/weixin/dto"
	"ginfast/api/weixin/model"
	"ginfast/pkg/ginx"

	"github.com/gin-gonic/gin"
)

// Msg 微信消息
type Msg struct {
}

// PageHandler 列表
func (Msg) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Msg
	total, _ := p.DbModelWhere(&model.Msg{}).Find(&data)
	ctx.WriteTotal(total, data)
}

func (o Msg) Routers(r *gin.RouterGroup) {
	r.GET("/page", o.PageHandler)
}

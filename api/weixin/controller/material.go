package controller

import (
	"ginfast/api/weixin/dto"
	"ginfast/api/weixin/model"
	"ginfast/pkg/ginx"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Material 素材
type Material struct {
}

// PageHandler 列表
func (Material) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Material
	total, _ := p.DbModelWhere(&model.Material{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// DeleteHandler 删除
func (Material) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Material{}, ginx.JSON(c))
}

type materialUpload struct {
	MediaType    string `form:"mediaType"`
	Title        string `form:"title"`
	Introduction string `form:"introduction"`
}

// UploadFileHandler 上传
func (Material) UploadFileHandler(c *gin.Context) {
	// var p materialUpload
	// if err := c.ShouldBind(&p); err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// path := "/public/wx/material/"
	// saveDir := configs.Default.Local.ServeRootPath + path
	// os.MkdirAll(saveDir, os.ModePerm)
	// var (
	// 	fileName string
	// 	fileType string
	// 	fileHead *multipart.FileHeader
	// 	err      error = errors.New("invaild type")
	// )
	// switch p.MediaType {
	// case "image":
	// 	fileHead, err = c.FormFile("file")
	// 	fileType = ".png"
	// case "video":
	// case "music":
	// case "news":
	// default:
	// 	return
	// }
	// if err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// if p.Title == "" {
	// 	curdate := time.Now().UnixNano()
	// 	fileName = strconv.FormatInt(curdate, 10) + fileType
	// } else {
	// 	fileName = p.Title + fileType
	// }
	// dts := saveDir + fileName
	// if err := c.SaveUploadedFile(fileHead, dts); err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// fileURL := configs.Default.Local.ServeRoot + path + fileName
	// material := &model.WxMaterial{}
	// material.URL = fileURL
	// material.Tag = p.MediaType
	// material.Name = p.Title
	// if err := orm.DbCreate(material); err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// ginx.JSONOK().WriteData(gin.H{
	// 	"data": gin.H{
	// 		"name":  fileName,
	// 		"label": p.Title,
	// 		"value": fileURL,
	// 	}}, c)
}

// UploadNewsHandler 图文
func (Material) UploadNewsHandler(c *gin.Context) {
}

func (o Material) Routers(r *gin.RouterGroup) {
	r.GET("/page", o.PageHandler)
	r.DELETE("", o.DeleteHandler)
	r.POST("/materialFileUpload", o.UploadFileHandler)
	r.POST("/materialNews", o.UploadNewsHandler)
}

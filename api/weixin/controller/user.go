package controller

import (
	"ginfast/api/weixin/dto"
	"ginfast/api/weixin/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// User 用户
type User struct {
}

// PageHandler 列表
func (User) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.User
	total, _ := p.DbModelWhere(&model.User{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (User) GetHandler(c *gin.Context) {
	util.QueryByID(&model.User{}, ginx.JSON(c))
}

// AddHandler 新增
func (User) AddHandler(c *gin.Context) {
	var p model.User
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateRemarkHandler 更新
func (User) UpdateRemarkHandler(c *gin.Context) {
	var p model.User
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateFields(&p, "Remark")
	ctx.WriteError(err)
}

func (o User) Routers(r *gin.RouterGroup) {
	r.GET("", o.AddHandler)
	r.GET("/page", o.PageHandler)
	r.POST("", o.AddHandler)
	r.PUT("/remark", o.UpdateRemarkHandler)
}

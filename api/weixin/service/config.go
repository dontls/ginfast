package service

import (
	"errors"
	"ginfast/api/weixin/dto"
	"ginfast/api/weixin/model"
	"ginfast/pkg/orm"
	"ginfast/util"
)

// AppConfig 获取小程序配置参数
func AppConfig() (*dto.AppCfg, error) {
	var data model.Config
	if err := orm.DbFirstBy(&data, "name like ?", dto.AppCfgName); err != nil {
		return nil, errors.New("please set app information firstly")
	}
	var cfg dto.AppCfg
	util.StringVal(data.Value, &cfg)
	return &cfg, nil
}

func OfficailConfig() (*dto.OfficialCfg, error) {
	var data model.Config
	if err := orm.DbFirstBy(&data, "name like ?", dto.OfficialCfgName); err != nil {
		return nil, errors.New("please set app information firstly")
	}
	var cfg dto.OfficialCfg
	util.StringVal(data.Value, &cfg)
	return &cfg, nil
}

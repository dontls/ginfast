package model

import "ginfast/pkg/orm"

// MaterialOpt 操作请求(修改)
type MaterialOpt struct {
	ID    uint   `json:"mediaId" gorm:"primary_key"`
	URL   string `json:"url" gorm:"default:'';"`
	Thumb string `json:"thumb" gorm:"default:'';"`
	Tag   string `json:"tag" gorm:"default:'';"`
	Name  string `json:"name" gorm:"default:'';"`
}

type Material struct {
	MaterialOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Material) TableName() string {
	return "t_WeixinMaterial"
}

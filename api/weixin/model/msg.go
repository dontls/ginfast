package model

import "ginfast/pkg/orm"

// Msg 微信消息
type MsgOpt struct {
	ID              uint    `json:"id" gorm:"primary_key"`
	CreateID        uint    `json:"createId"`                                     // gorm:"comment:'创建者'";
	UpdateID        uint    `json:"updateId"`                                     // gorm:"comment:'更新者';"
	Remark          string  `json:"remark"`                                       // gorm:"comment:'备注';"
	AppName         string  `json:"appName" gorm:"default:'';type:varchar(64)"`   // comment:'公众号名称';
	AppLogo         string  `json:"appLogo" gorm:"default:'';"`                   // comment:'公众号logo';
	NickName        string  `json:"nickName" gorm:"default:'';type:varchar(64)"`  // comment:'昵称';
	HeadimgURL      string  `json:"headimgUrl"`                                   // comment:'头像';
	Type            string  `json:"type" gorm:"default:'';type:varchar(1)"`       // ;comment:'消息分类（1、用户发给公众号；2、公众号发给用户；）'
	RepType         string  `json:"repType" gorm:"default:'';type:varchar(32);"`  // 消息类型（text：文本；image：图片；voice：语音；video：视频；shortvideo：小视频；location：地理位置；music：音乐；news：图文；event：推送事件）
	RepEvent        string  `json:"repEvent" gorm:"default:'';type:varchar(32);"` // 事件类型（subscribe：关注；unsubscribe：取关；CLICK、VIEW：菜单事件）
	RepContent      string  `json:"repContent" gorm:"default:'';"`                // 回复类型文本保存文字
	RepMediaID      string  `json:"repMediaId" gorm:"default:'';"`                // 回复类型imge、voice、news、video的mediaID或音乐缩略图的媒体id
	RepName         string  `json:"repName" gorm:"default:'';"`                   // 回复的素材名、视频和音乐的标题
	RepDesc         string  `json:"repDesc" gorm:"default:'';"`                   // 视频和音乐的描述
	RepURL          string  `json:"repUrl" gorm:"default:'';"`                    // 链接
	RepHqURL        string  `json:"repHqUrl" gorm:"default:'';"`                  // 高质量链接
	Content         string  `json:"content" gorm:"default:'';"`                   // 图文消息的内容
	RepThumbMediaID string  `json:"repThumbMediaId" gorm:"default:'';"`           // 缩略图的媒体id
	RepThumbURL     string  `json:"repThumbUrl" gorm:"default:'';"`               // 缩略图url
	RepLocationX    float64 `json:"repLocationX" gorm:"default:0.00;"`            // 地理位置维度
	RepLocationY    float64 `json:"repLocationY" gorm:"default:0.00;"`            // 地理位置经度
	RepScale        float64 `json:"repScale" gorm:"default:0.00;"`                // 地图缩放大小
	ReadFlag        string  `json:"readFlag" gorm:"default:'';type:varchar(1)"`   // ;comment:'已读标记（0：是；1：否）'
}

type Msg struct {
	MsgOpt
	orm.ModelTime
}

func (Msg) TableName() string {
	return "t_WeixinMsg"
}

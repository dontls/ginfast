package model

import "ginfast/pkg/orm"

type Config struct {
	ID    uint   `json:"id" gorm:"primary_key"`
	Name  string `json:"name" gorm:";default:'';comment:名称;"` // 参数定义
	Value string `json:"value" gorm:"type:varchar(2048);comment:参数键值;"`
	orm.ModelTime
	orm.ModelAuth
}

func (Config) TableName() string {
	return "t_WeixinConfig"
}

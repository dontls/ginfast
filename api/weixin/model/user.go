package model

import "ginfast/pkg/orm"

// UserOpt 操作请求(获取/修改/更新)
type UserOpt struct {
	ID        uint   `json:"id" gorm:"primary_key"`
	Status    string `json:"status" gorm:"type:char(1);default:'0';comment:帐号状态 0:正常 1:停用;"`
	AppType   string `json:"appType" gorm:"type:char(1);default:'2';comment:应用类型 1:小程序 2:公众号;"`
	Subscribe string `json:"subscribe" gorm:"type:char(1);default:'0';comment:是否订阅 0:是 1:否 2:网页授权用户;"`
	// 返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，
	// ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENEPROFILE LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他
	SubscribeScene      string  `json:"subscribeScene"`
	SubscribeTime       string  `json:"subscribeTime" gorm:"comment:关注时间;"`
	SubscribeNum        uint    `json:"subscribeNum" gorm:"comment:关注次数;"`
	CancelSubscribeTime string  `json:"cancelSubscribeTime" gorm:"comment:取消关注时间;"`
	OpenId              string  `json:"openId" gorm:"comment:用户标识;"`
	UserName            string  `json:"userName" gorm:"comment:名称;"`
	NickName            string  `json:"nickName" gorm:"comment:昵称;"`
	Sex                 string  `json:"sex" gorm:"type:char(1);comment:性别 1:男 2:女 0:未知;"`
	City                string  `json:"city" gorm:"comment:所在城市;"`
	Country             string  `json:"country" gorm:"comment:所在国家;"`
	Province            string  `json:"province" gorm:"comment:所在省份;"`
	Phonenumber         string  `json:"phonenumber" gorm:"type:char(11);comment:手机号码;"`
	Language            string  `json:"language" gorm:"type:char(64);comment:用户语言;"`
	HeadimgURL          string  `json:"headimgUrl" gorm:"comment:头像;"`
	UnionId             string  `json:"unionId" gorm:"type:char(64); comment:union_id;"`
	GroupId             string  `json:"groupId" gorm:"type:char(64); comment:用户组;"`
	QrSceneStr          string  `json:"qrSceneStr" gorm:"type:char(64); omment:二维码扫码场景;"`
	Latitude            float32 `json:"latitude" gorm:"comment:地理位置纬度;"`
	Longitude           float32 `json:"longitude" gorm:"comment:地理位置经度;"`
	Precision           float32 `json:"precision" gorm:"comment:地理位置精度;"`
	SessionKey          string  `json:"sessionKey" gorm:"type:char(64);comment:会话密钥'"`
	Remark              string  `json:"remark" gorm:"type:text;"`
}

// User 微信用户
type User struct {
	UserOpt
	orm.ModelAuth
	orm.ModelTime
}

func (User) TableName() string {
	return "t_WeixinUser"
}

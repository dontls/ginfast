package weixin

import (
	"ginfast/api/weixin/controller"
	"ginfast/api/weixin/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"

	"github.com/gin-gonic/gin"
)

func init() {
	orm.RegisterModel(&model.Config{})
	ginx.Register("/wechat", func(r *gin.RouterGroup) {
		controller.WeChat{}.Routers(r)
	})
	ginx.RegisterAuth("/weixin", func(r *gin.RouterGroup) {
		controller.Config{}.Routers(r.Group("/config"))
		controller.User{}.Routers(r.Group("/user"))
		controller.Material{}.Routers(r.Group("/material"))
		controller.Msg{}.Routers(r.Group("/msg"))
	})
}

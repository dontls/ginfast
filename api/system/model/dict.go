package model

import "ginfast/pkg/orm"

// DictDataOpt 操作请求(修改/更新)
type DictDataOpt struct {
	ID        uint   `json:"dictCode" gorm:"primary_key"`
	Remark    string `json:"remark"`
	Sort      int    `json:"dictSort"`  // 字典排序
	Label     string `json:"dictLabel"` // 字典标签
	Value     string `json:"dictValue"` // 字典键值
	Type      string `json:"dictType"`  // 字典类型
	CSSClass  string `json:"cssClass"`  // 样式属性（其他样式扩展）
	ListClass string `json:"listClass"` // 表格字典样式
	Status    *int   `json:"status" gorm:"default:1;gorm:1-启用 0禁用;"`
	IsDefault uint   `json:"isDefault" gorm:"default:1;comment:状态:1=是,0=否"`
}

// DictData 字典
type DictData struct {
	DictDataOpt
	orm.ModelTime
}

func (DictData) TableName() string {
	return "t_SystemDictData"
}

// DictTypeOpt 操作请求(修改/更新)
type DictTypeOpt struct {
	ID     uint   `json:"dictId" gorm:"primary_key"`
	Status *int   `json:"status" gorm:"default:1;gorm:1-启用 0禁用;"`
	Remark string `json:"remark"`
	Name   string `json:"dictName"`
	Type   string `json:"dictType"`
}

// DictType 字典
type DictType struct {
	DictTypeOpt
	orm.ModelTime
}

func (DictType) TableName() string {
	return "t_SystemDicType"
}

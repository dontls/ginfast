package model

import "ginfast/pkg/orm"

// Menu 权限
type MenuOpt struct {
	ID          uint   `json:"id" gorm:"primary_key"`
	Pid         uint   `json:"pid"`         // 父ID
	Title       string `json:"title"`       // 菜单名称
	Api         string `json:"api"`         // 接口路径
	Icon        string `json:"icon"`        // 图标
	Condition   string `json:"condition"`   // 条件
	Remark      string `json:"remark"`      // 备注
	MenuType    uint   `json:"menuType"`    // 类型 0目录 1菜单 2按钮
	Weigh       int    `json:"weigh"`       // 权重
	IsHide      string `json:"isHide"`      // 显示状态
	IsKeepAlive uint   `json:"isKeepAlive"` // 是否缓存
	IsAffix     uint   `json:"isAffix"`     // 是否固定
	Path        string `json:"path"`        // 路由地址
	Redirect    string `json:"redirect"`    // 跳转路由
	Component   string `json:"component"`   // 组件路径
	IsIframe    uint   `json:"isIframe"`    // 是否iframe
	IsLink      uint   `json:"isLink"`      // 是否外链 1是 0否
	LinkUrl     string `json:"linkUrl"`     // 链接地址
}

type Menu struct {
	MenuOpt
	orm.ModelTime
	Children []Menu `json:"children,omitempty" gorm:"-"` //
}

func (Menu) TableName() string {
	return "t_SystemMenu"
}

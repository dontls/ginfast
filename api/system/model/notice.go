package model

import "ginfast/pkg/orm"

// NoticeOpt 操作请求(获取/修改/更新)
type NoticeOpt struct {
	ID      uint   `json:"noticeId" gorm:"primary_key"`
	Title   string `json:"noticeTitle" gorm:"comment:公告标题;"`
	Type    string `json:"noticeType" gorm:"comment:公告类型（1通知 2公告）;"`
	Content string `json:"noticeContent" gorm:"size:500;comment:公告内容;"`
	Status  string `json:"status" gorm:"default:'0';type:varchar(1);gorm:公告状态（0正常 1关闭）;"`
}

// Config 参数
type Notice struct {
	NoticeOpt
	orm.ModelTime
}

func (Notice) TableName() string {
	return "t_SystemNotice"
}

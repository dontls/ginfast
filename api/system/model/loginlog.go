package model

import "ginfast/pkg/orm"

// LoginLog is the golang structure for table t_system_loginlog.
type LoginLog struct {
	ID        uint     `json:"infoId"`        // 访问ID
	Name      string   `json:"loginName"`     // 登录账号
	Ipaddr    string   `json:"ipaddr"`        // 登录IP地址
	Location  string   `json:"loginLocation"` // 登录地点
	Browser   string   `json:"browser"`       // 浏览器类型
	Os        string   `json:"os"`            // 操作系统
	Status    int      `json:"status"`        // 登录状态（0成功 1失败）
	Msg       string   `json:"msg"`           // 提示消息
	CreatedAt orm.Time `json:"loginTime"`     // 登录时间
	Module    string   `json:"module"`        // 登录模块
}

func (LoginLog) TableName() string {
	return "t_SystemLoginLog"
}

package model

import "ginfast/pkg/orm"

type PostOpt struct {
	ID     uint   `json:"postId" gorm:"primary_key"`
	Code   string `json:"postCode" gorm:"comment:岗位编码"`
	Name   string `json:"postName" gorm:"comment:岗位名称"`
	Sort   int    `json:"postSort" gorm:"comment:岗位排序"`
	Status *int   `json:"status" gorm:"default:1;gorm:1-启用 0禁用;"`
	Remark string `json:"remark" gorm:"comment:备注"`
}

// Post 岗位
type Post struct {
	PostOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Post) TableName() string {
	return "t_SystemPost"
}

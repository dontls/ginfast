package model

import "ginfast/pkg/orm"

// RoleOpt 操作请求(获取/修改/更新)
type RoleOpt struct {
	ID        uint          `json:"roleId" gorm:"primary_key"`
	Name      string        `json:"roleName" gorm:"not null;comment:角色名称;"`
	Key       string        `json:"roleKey" gorm:"size:100;not null;comment:角色权限字符串;"`
	Order     int           `json:"roleOrder" gorm:"not null;comment:显示顺序;"`
	DataScope string        `json:"dataScope" gorm:"default:1;comment:数据范围 1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限;"`
	Status    *int          `json:"status" gorm:"default:1;gorm:1-启用 0禁用;"`
	Remark    string        `json:"remark" gorm:"size:500;"`
	MenuIds   orm.UintArray `json:"menuIds"`
}

// Role 角色
// 下级角色权限最多和上级角色一样
type Role struct {
	RoleOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Role) TableName() string {
	return "t_SystemRole"
}

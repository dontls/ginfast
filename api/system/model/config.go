package model

import "ginfast/pkg/orm"

// ConfigOpt 操作请求(获取/修改/更新)
type ConfigOpt struct {
	ID      uint   `json:"configId" gorm:"primary_key"`
	Name    string `json:"configName" gorm:"comment:参数名称;"`
	Keyword string `json:"configKey" gorm:"comment:参数键名;"`
	Value   string `json:"configValue" gorm:"comment:参数键值;"`
	Type    string `json:"configType" gorm:"comment:1=是,0=否;"`
}

// Config 参数
type Config struct {
	ConfigOpt
	orm.ModelTime
}

func (Config) TableName() string {
	return "t_SystemConfig"
}

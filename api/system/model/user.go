package model

import "ginfast/pkg/orm"

const (
	// UserRoleID 系统用户角色
	UserRoleID = 0
	// UserName 系统用户名
	UserName = "admin"
)

// UserOpt 操作请求(获取/修改/更新)
type UserOpt struct {
	ID       uint          `json:"userId" gorm:"primary_key"`
	UserName string        `json:"userName" gorm:"not null;comment:登录账号;"`
	NickName string        `json:"nickName" gorm:"not null;comment:用户昵称;"`
	Email    string        `json:"email" gorm:"size:50;comment:用户邮箱;"`
	Mobile   string        `json:"mobile" gorm:"size:11;default:'';comment:手机号码;"`
	Sex      string        `json:"sex" gorm:"size:1;default:'0';comment:用户性别(0男 1女 2未知);"`
	Avatar   string        `json:"avatar" gorm:"size:50;comment:头像路径;"`
	Password string        `json:"-" gorm:"size:50;not null;comment:密码;"`
	Salt     string        `json:"-" gorm:"size:50;not null;comment:盐加密;"`
	Status   *int          `json:"status" gorm:"default:1;gorm:1-启用 0-禁用;"`
	DeptID   uint          `json:"deptId" gorm:"comment:部门ID;"`
	Dept     *Dept         `json:"dept" gorm:"foreignKey:DeptID"`
	RoleID   uint          `json:"roleId" gorm:"not null pk;comment:当前使用角色ID;"`
	RoleIDs  orm.UintArray `json:"roleIds" gorm:"not null pk;comment:角色ID;"`
	PostIDs  orm.UintArray `json:"postIds" gorm:"comment:岗位ID;"`
	Remark   string        `json:"remark" gorm:"size:500;comment:备注;"`
}

// User 用户
type User struct {
	UserOpt
	orm.ModelAuth
	orm.ModelTime
}

func (User) TableName() string {
	return "t_SystemUser"
}

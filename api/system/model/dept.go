package model

import "ginfast/pkg/orm"

// DeptOpt 操作请求(获取/修改/更新)
type DeptOpt struct {
	ID       uint   `json:"deptId" gorm:"primary_key"`
	ParentID uint   `json:"parentId" gorm:"default:0"`                 //  comment('父部门id') BIGINT(20)"
	Name     string `json:"deptName" gorm:"default:'';type:char(32);"` //  comment('部门名称')
	Num      int    `json:"orderNum" gorm:"default:0"`                 // comment('显示顺序')
	Leader   string `json:"leader" gorm:"type:char(64);"`              // comment('负责人')
	Phone    string `json:"phone" gorm:"type:char(11)"`                // comment('联系电话')
	Email    string `json:"email"`                                     // comment('邮箱')
	Status   *int   `json:"status" gorm:"default:1;gorm:1-启用 0禁用;"`
}

// Dept 部门
type Dept struct {
	DeptOpt
	orm.ModelTime
}

func (Dept) TableName() string {
	return "t_SystemDept"
}

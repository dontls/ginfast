package controller

import (
	"errors"
	"ginfast/api/middleware"
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/api/system/service"
	"ginfast/configs"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// User
type User struct {
}

// ListHandler 用户列表
func (User) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	t := middleware.GetUser(c)
	if t.UserName != model.UserName {
		p.CreatedBy = t.UserName
	}
	var data []model.User
	total, _ := p.DbModelWhere(&model.User{}).Preload("Dept").Find(&data)
	ctx.WriteTotal(total, data)
}

// MenuHandler
func (User) MenuHandler(c *gin.Context) {
	t := middleware.GetUser(c)
	menulist, permissions := service.MenuRouter(t.RoleID)
	ginx.JSON(c).WriteData(gin.H{"menuList": menulist, "permissions": permissions})
}

type userRole struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type userPost struct {
	ID   uint   `json:"postId"`
	Name string `json:"postName"`
}

// ParamsHandler
func (User) ParamsHandler(c *gin.Context) {
	t := middleware.GetUser(c)
	var roles []userRole
	orm.DB().Model(&model.Role{}).Where("CreatedBy = ?", t.UserName).Scan(&roles)
	var posts []userPost
	orm.DB().Model(&model.Post{}).Scan(&posts)
	ginx.JSON(c).WriteData(gin.H{"roleList": roles, "posts": posts})
}

// GetHandler
func (User) GetHandler(c *gin.Context) {
	ctx, queryId := ginx.JSON(c).ParamUInt("id")
	var user model.User
	err := orm.DbFirstByID(&user, queryId)
	ctx.WriteData(gin.H{"data": user, "roleIds": user.RoleIDs}, err)
}

// AuthRoleHandler
func (User) AuthRoleHandler(c *gin.Context) {
	ctx, id := ginx.JSON(c).ParamUInt("id")
	var user model.User
	var roles []model.Role
	err := orm.DbFirstByID(&user, id)
	if err == nil {
		t := middleware.GetUser(c)
		orm.DbFindBy(&roles, "CreatedBy LIKE ?", t.UserName) // 获取当前用户创建的角色
	}
	ctx.WriteData(gin.H{"roles": roles, "user": user})
}

// AddHandler 新增用户
func (User) AddHandler(c *gin.Context) {
	var p model.User
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	p.CreatedBy = middleware.GetUser(c).UserName
	err = service.UserCreate(&p)
	ctx.WriteError(err)
}

// AddPageHandler 新增界面
func (User) AddPageHandler(c *gin.Context) {
	// trees, err := buildRoleTree(c)
	// if err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// var posts []model.Post
	// if err := orm.DbFind(&posts); err != nil {
	// 	ginx.JSONWriteError(err, c)
	// 	return
	// }
	// ginx.JSONWriteData(gin.H{"roles": trees, "posts": posts}, c)
}

// ExportHandler 导出
func (User) ExportHandler(c *gin.Context) {
	// var users []model.User
	// totalCount, err := orm.DbFindPage(param.PageNum, param.PageSize, param.Where(User), &model.User{}, &users)
	ginx.JSON(c).SetCode(ginx.StatusError).WriteData(nil)
}

// ProfileHandler profile
func (User) ProfileHandler(c *gin.Context) {
	t := middleware.GetUser(c)
	var user model.User
	err := orm.DbFirstByID(&user, t.UserID)
	ginx.JSON(c).WriteData(gin.H{"data": user, "roleGroup": "", "postGroup": ""}, err)
}

type updatePwd struct {
	OldPassword string `form:"oldPassword" binding:"required,min=0,max=30"`
	NewPassword string `form:"newPassword" binding:"required,min=0,max=30"`
}

// ProfileUpdatePwdHandler 重置密码
func (User) ProfileUpdatePwdHandler(c *gin.Context) {
	var p updatePwd
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	t := middleware.GetUser(c)
	var data model.User
	if err := orm.DbFirstByID(&data, t.UserID); err != nil {
		ctx.WriteError(err)
		return
	}
	oldPassword := service.UserPassword(&data, p.OldPassword)
	if oldPassword != data.Password {
		ctx.WriteError(errors.New("old password error"))
		return
	}
	data.Password = service.UserPassword(&data, p.NewPassword)
	err = orm.DbUpdateFields(&data, "Password")
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (User) UpdateHandler(c *gin.Context) {
	var p model.User
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	if p.UserName == model.UserName {
		p.RoleIDs = nil
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

type resetPwd struct {
	UserID   uint   `json:"userId"`
	Password string `json:"password"`
}

// ResetPwdHandler 修改密码
func (User) ResetPwdHandler(c *gin.Context) {
	var p resetPwd
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data model.User
	if err := orm.DbFirstByID(&data, p.UserID); err != nil {
		ctx.WriteError(err)
		return
	}
	data.Password = service.UserPassword(&data, p.Password)
	err = orm.DbUpdateFields(&data, "Password")
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (User) DeleteHandler(c *gin.Context) {
	ctx, idstr := ginx.MustParam(c, "id")
	if idstr == "" {
		return
	}
	ids := util.StringSplit(idstr, ",")
	for i, v := range ids {
		if v == 1 {
			ids[i] = 0
			break
		}
	}
	err := orm.DbDeleteByIDs(&model.User{}, ids)
	ctx.WriteError(err)
}

// EnableHandler 改变状态
func (User) EnableHandler(c *gin.Context) {
	var p model.User
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	err = orm.DbUpdateFields(&p, "Status")
	ctx.WriteError(err)
}

// UpdatePwdHandler 重置密码
func (User) UpdatePwdHandler(c *gin.Context) {
	var p updatePwd
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	t := middleware.GetUser(c)
	var data model.User
	if err := orm.DbFirstByID(&data, t.UserID); err != nil {
		ctx.WriteError(err)
		return
	}
	oldPassword := service.UserPassword(&data, p.OldPassword)
	if oldPassword != data.Password {
		ctx.WriteError(err)
		return
	}
	data.Password = service.UserPassword(&data, p.NewPassword)
	err = orm.DbUpdateFields(&data, "Password")
	ctx.WriteError(err)
}

// ProfileAuatarHandler 上传头像
func (User) ProfileAuatarHandler(c *gin.Context) {
	t := middleware.GetUser(c)
	saveDir := configs.Public.Abs("upload")
	fileHead, err := c.FormFile("avatarfile")
	if err == nil {
		curdate := time.Now().UnixNano()
		filename := t.UserName + strconv.FormatInt(curdate, 10) + ".png"
		avatarURL := saveDir + filename
		err = c.SaveUploadedFile(fileHead, avatarURL)
		if err == nil {
			err = orm.DbUpdateByID(&model.User{}, t.UserID, orm.H{"Avatar": avatarURL})
		}
	}
	ginx.JSON(c).WriteError(err)
}

func (o User) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/menu", o.MenuHandler)
	r.GET("/params", o.ParamsHandler)
	r.GET("/:id", o.GetHandler)
	r.GET("/authRole/:id", o.AuthRoleHandler)
	r.POST("", o.AddHandler)
	r.GET("/export", o.ExportHandler)
	r.PUT("", o.UpdateHandler)
	r.PUT("/resetPwd", o.ResetPwdHandler)
	r.DELETE("/:id", o.DeleteHandler)
	r.PUT("/enable", o.EnableHandler)
	r.GET("/profile", o.ProfileHandler)
	r.PUT("/profile", o.UpdateHandler)
	r.PUT("/profile/updatePwd", o.UpdatePwdHandler)
	r.POST("/profile/avatar", o.ProfileAuatarHandler)
}

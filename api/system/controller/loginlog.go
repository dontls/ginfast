package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/api/system/service"
	"ginfast/pkg/ginx"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// LoginLog
type LoginLog struct {
}

// ListHandler 列表
func (LoginLog) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.LoginLog
	total, _ := p.DbModelWhere(&model.LoginLog{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// DeleteHandler 删除
func (LoginLog) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.LoginLog{}, ginx.JSON(c))
}

// ClearHandler 清空
func (LoginLog) ClearHandler(c *gin.Context) {
	service.Clear(&model.LoginLog{}, ginx.JSON(c))
}

func (o LoginLog) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.DELETE("/:id", o.DeleteHandler)
	r.DELETE("/clear", o.ClearHandler)
}

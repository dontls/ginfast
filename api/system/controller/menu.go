package controller

import (
	"ginfast/api/middleware"
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/api/system/service"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Menu 系统管理菜单
type Menu struct {
}

// ListHandler 菜单列表
func (Menu) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Menu
	p.DbModelWhere(&model.Menu{}).Find(&data)
	ctx.WriteData(data)
}

// TreeSelectHandler 列表
func (Menu) TreeSelectHandler(c *gin.Context) {
	data := service.MenuTree(middleware.GetUser(c).RoleID)
	ginx.JSON(c).WriteData(data)
}

// RoleMenuTreeHandler 角色菜单列表
func (Menu) RoleMenuTreeHandler(c *gin.Context) {
	ctx, id := ginx.JSON(c).ParamUInt("id")
	data := service.MenuTree(id)
	ctx.WriteData(gin.H{"menus": data})
}

// GetHandler 查询菜单详细
func (Menu) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Menu{}, ginx.JSON(c))
}

// AddHandler 新增
func (Menu) AddHandler(c *gin.Context) {
	var p model.Menu
	ctx, err := ginx.MustBind(c, &p.MenuOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Menu) UpdateHandler(c *gin.Context) {
	var p model.Menu
	ctx, err := ginx.MustBind(c, &p.MenuOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除菜单
func (Menu) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Menu{}, ginx.JSON(c))
}

func (o Menu) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.GET("/treeselect", o.TreeSelectHandler)
	r.GET("/roleMenuTreeselect/:id", o.RoleMenuTreeHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}

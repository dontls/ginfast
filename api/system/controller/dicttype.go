package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// DictType 系统管理字典类型
type DictType struct {
}

// ListHandler 字典类型列表
func (DictType) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.DictType
	total, _ := p.DbModelWhere(&model.DictType{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询字典详细
func (DictType) GetHandler(c *gin.Context) {
	util.QueryByID(&model.DictType{}, ginx.JSON(c))
}

// AddHandler 新增
func (DictType) AddHandler(c *gin.Context) {
	var p model.DictType
	//获取参数
	ctx, err := ginx.MustBind(c, &p.DictTypeOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改p
func (DictType) UpdateHandler(c *gin.Context) {
	var p model.DictType
	//获取参数
	ctx, err := ginx.MustBind(c, &p.DictTypeOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (DictType) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.DictType{}, ginx.JSON(c))
}

func (o DictType) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}

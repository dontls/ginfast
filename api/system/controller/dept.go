package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/pkg/tree"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Dept 部门
type Dept struct {
}

// ListHandler 列表
func (Dept) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Dept
	p.DbModelWhere(&model.Dept{}).Find(&data)
	ctx.WriteData(data)
}

// ListExcludeHandler 部门列表（排除节点）
func (Dept) ListExcludeHandler(c *gin.Context) {
	// id, err := ctxQueryInt(c, "id")
	// if err != nil {
	// 	JSONP(StatusError).WriteTo(c)
	// }
	// where := fmt.Sprintf("id != %d", id)
	// var depts []model.Dept
	// orm.DbFindAll(where, depts, "order_num asc")
	ginx.JSON(c).WriteData(nil)
}

// GetHandler 查询部门详细
func (Dept) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Dept{}, ginx.JSON(c))
}

// TreeSelectHandler 查询下拉树结构
func (Dept) TreeSelectHandler(c *gin.Context) {
	var nodes []dto.Node
	err := orm.DB().Model(&model.Dept{}).Scan(&nodes).Error
	if err == nil {
		tree.Slice(nodes, func(i int) bool { return nodes[i].IsRoot(nodes) }, func(i1, i2 int) bool {
			return nodes[i1].ID == nodes[i2].ParentID
		})
	}
	ginx.JSON(c).WriteData(nodes, err)
}

// RoleDeptTreeSelectHandler 根据角色ID查询树结构
func (Dept) RoleDeptTreeSelectHandler(c *gin.Context) {
	ginx.JSON(c).WriteData(nil)
}

func checkAddDept(s *model.Dept) error {
	if err := orm.DbFirstBy(&model.Dept{}, "Name LIKE ?", s.Name); err != nil {
		return err
	}
	return nil
}

// AddHandler 新增
func (Dept) AddHandler(c *gin.Context) {
	var p model.Dept
	//获取参数p
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	err = checkAddDept(&p)
	if err == nil {
		err = orm.DbCreate(&p)
	}
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Dept) UpdateHandler(c *gin.Context) {
	var p model.Dept
	ctx, err := ginx.MustBind(c, &p.DeptOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Dept) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Dept{}, ginx.JSON(c))
}

func (o Dept) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/list/exclude/:id", o.ListExcludeHandler)
	r.GET("/:id", o.GetHandler)
	r.GET("/treeSelect", o.TreeSelectHandler)
	r.GET("/roleDeptTreeselect/:id", o.RoleDeptTreeSelectHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}

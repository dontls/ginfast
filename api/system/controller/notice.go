package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Notice 通知
type Notice struct {
}

// ListHandler 列表
func (Notice) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Notice
	total, _ := p.DbModelWhere(&model.Notice{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 获取指定id
func (Notice) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Notice{}, ginx.JSON(c))
}

// AddHandler 新增
func (Notice) AddHandler(c *gin.Context) {
	var p model.Notice
	//获取参数
	ctx, err := ginx.MustBind(c, &p.NoticeOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Notice) UpdateHandler(c *gin.Context) {
	var p model.Notice
	//获取参数
	ctx, err := ginx.MustBind(c, &p.NoticeOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Notice) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Notice{}, ginx.JSON(c))
}

func (o Notice) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}

package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Post 岗位
type Post struct {
}

// ListHandler 字典类型列表
func (Post) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Post
	total, _ := p.DbModelWhere(&model.Post{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询字典详细
func (Post) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Post{}, ginx.JSON(c))
}

// AddHandler 新增
func (Post) AddHandler(c *gin.Context) {
	var p model.Post
	//获取参数
	ctx, err := ginx.MustBind(c, &p.PostOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Post) UpdateHandler(c *gin.Context) {
	var p model.Post
	//获取参数
	ctx, err := ginx.MustBind(c, &p.PostOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Post) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Post{}, ginx.JSON(c))
}

func (o Post) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}

package controller

import (
	"ginfast/api/system/dto"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Config 配置
type Config struct {
}

// ListHandler 列表
func (Config) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Config
	total, _ := p.DbModelWhere(&model.Config{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 获取指定id
func (Config) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Config{}, ginx.JSON(c))
}

// AddHandler 新增
func (Config) AddHandler(c *gin.Context) {
	var p model.Config
	//获取参数
	ctx, err := ginx.MustBind(c, &p.ConfigOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Config) UpdateHandler(c *gin.Context) {
	var p model.Config
	//获取参数
	ctx, err := ginx.MustBind(c, &p.ConfigOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Config) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Config{}, ginx.JSON(c))
}

// InitPasswordHandler 配置密码
func (Config) InitPasswordHandler(c *gin.Context) {
	ginx.JSON(c).WriteData(nil)
}

func (o Config) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
	r.GET("/configKey/sys.user.initPassword", o.InitPasswordHandler)
}

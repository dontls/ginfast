package service

import (
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"

	"gorm.io/gorm"
)

func Clear(v interface{}, c *ginx.Context) {
	orm.DB().Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(v)
	c.WriteData(nil)
}

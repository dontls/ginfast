package service

import (
	"ginfast/api/system/model"
	"ginfast/pkg/orm"
	"ginfast/pkg/tree"
)

// MenusByRoleID 查询指定权限byID
func MenusByRoleID(roleID uint, v interface{}, query interface{}, args ...interface{}) error {
	db := orm.DB().Model(&model.Menu{})
	if query != nil {
		db = db.Where(query, args...)
	}
	if roleID == model.UserRoleID {
		return db.Scan(v).Error
	}
	var role model.Role
	if err := orm.DbFirstByID(&role, roleID); err != nil {
		return err
	}
	return db.Where("id in (?)", role.MenuIds).Scan(v).Error
}

// Meta 路由显示信息
type Meta struct {
	Title       string `json:"title"`       // 规则名称
	Icon        string `json:"icon"`        // 图标
	IsHide      bool   `json:"isHide"`      // 显示状态
	IsKeepalive bool   `json:"isKeepAlive"` // 是否缓存
	IsAffix     bool   `json:"isAffix"`     // 是否固定
	IsIframe    bool   `json:"isIframe"`    // 是否iframe
	IsLink      uint   `json:"isLink"`      // 是否外链 1是 0否
}

// Router 路由
type Router struct {
	ID        uint          `json:"id"`
	Pid       uint          `json:"pid"`
	Name      string        `json:"name"`      // 路由名字
	Path      string        `json:"path"`      // 路由地址
	Component string        `json:"component"` // 组件地址
	Meta      `json:"meta"` // 其他元素
	Children  []interface{} `json:"children,omitempty" gorm:"-"` // 子路由
}

func MenuRouter(roleId uint) (r interface{}, perms []string) {
	var rs []Router
	if err := MenusByRoleID(roleId, &rs, "menu_type < 2"); err != nil {
		return
	}
	if roleId == model.UserRoleID {
		perms = append(perms, "*/*/*")
	}
	r = tree.Slice(rs, func(i int) bool { return rs[i].Pid == 0 },
		func(i1, i2 int) bool { return rs[i1].ID == rs[i2].Pid })
	return
}

func MenuTree(roleId uint) interface{} {
	var menus []model.Menu
	if err := MenusByRoleID(roleId, &menus, nil); err != nil {
		return nil
	}
	return tree.Slice(menus, func(i int) bool { return menus[i].Pid == 0 },
		func(i1, i2 int) bool { return menus[i1].ID == menus[i2].Pid })
}

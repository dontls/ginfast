package dto

import (
	"ginfast/api/system/model"
	"ginfast/pkg/orm"
)

type Where struct {
	*orm.DbPage
	Status    string   `form:"status"` // 菜单状态
	DateRange []string `form:"dateRange[]"`

	Title     string `form:"title"`     // 菜单名称
	Component string `form:"component"` // 菜单名称

	RoleName string `form:"roleName"` // 角色名称
	RoleID   uint   `form:"roleId"`   //

	KeyWords string `form:"keyWords"` // 用户名
	Mobile   string `form:"mobile"`   // 手机号

	DictName  string `form:"dictName"`  // 字典标签
	DictType  string `form:"dictType"`  // 字典名称
	DictLabel string `form:"dictLabel"` // 字典标签

	DeptName string `form:"deptName"` // 部门名称
	DeptID   string `form:"deptId"`   // 部门ID

	PostName string `form:"postName"` // 岗位名称
	PostCode string `form:"postCode"` // 岗位名称

	ConfigName string `form:"configName"` //
	ConfigKey  string `form:"configKey"`  //
	ConfigType string `form:"configType"` //

	NoticeTitle string `form:"noticeTitle"` //
	CreatedBy   string `form:"createdBy"`   //

	IpAddr        string `form:"ipaddr"`
	LoginLocation string `form:"loginLocation"`
	UserName      string `form:"userName"`

	Service string `form:"service"`
	Model   string `form:"model"`
}

func (p *Where) DbModelWhere(v interface{}, handles ...func(dw *orm.DbWhere)) *orm.DbWhere {
	w := p.DbWhere()
	switch v.(type) {
	case *model.Menu:
		w.Like("Title", p.Title)
		w.Like("Component", p.Component)
	case *model.Role:
		w.EqualNumber("Status", p.Status)
		w.Equal("CreatedBy", p.CreatedBy)
		w.Like("Name", p.RoleName)
	case *model.User:
		w.EqualNumber("Status", p.Status)
		w.DateRange("CreatedAt", p.DateRange)
		w.Like("Mobile", p.Mobile)
		w.EqualNumber("DeptID", p.DeptID)
		if p.KeyWords != "" {
			w.Add("UserName LIKE ? OR NickName LIKE ?", "%"+p.KeyWords+"%", "%"+p.KeyWords+"%")
		}
	case *model.DictData, *model.DictType:
		w.EqualNumber("Status", p.Status)
		w.DateRange("CreatedAt", p.DateRange)
		w.Equal("Label", p.DictLabel)
		w.Like("Name", p.DictName)
		w.Like("Type", p.DictType)
	case *model.Dept:
		w.EqualNumber("Status", p.Status)
		w.Like("Name", p.DeptName)
	case *model.Post:
		w.EqualNumber("Status", p.Status)
		w.Like("Name", p.PostName)
		w.Like("Code", p.PostCode)
	case *model.Config:
		w.EqualNumber("Type", p.ConfigType)
		w.DateRange("CreatedAt", p.DateRange)
		w.Like("Name", p.ConfigName)
		w.Like("Keyword", p.ConfigKey)
	case *model.Notice:
		w.Like("NoticeTitle", p.NoticeTitle)
		w.Like("CreatedBy", p.CreatedBy)
	case *model.LoginLog:
		w.EqualNumber("Status", p.Status)
		w.DateRange("CreatedAt", p.DateRange)
		w.Like("Mame", p.UserName)
		w.Like("Locat", p.LoginLocation)
		w.Orders = append(w.Orders, "CreatedAt desc")
	}
	for _, hle := range handles {
		hle(w)
	}
	return w.Model(v)
}

type Node struct {
	ID       uint   `json:"id"`       // ID
	ParentID uint   `json:"parentId"` // 上级ID
	Name     string `json:"label"`
	Children []Node `json:"children,omitempty" gorm:"-"`
}

func (o *Node) IsRoot(b []Node) bool {
	for _, v := range b {
		if v.ID == o.ParentID {
			return false
		}
	}
	return true // 自动判断是否存在上级节点
}

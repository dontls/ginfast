package system

import (
	"ginfast/api/system/controller"
	"ginfast/api/system/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"

	"github.com/gin-gonic/gin"
)

func init() {
	orm.RegisterModel(
		&model.User{},
		&model.Role{},
		&model.Menu{},
		&model.Dept{},
		&model.DictType{},
		&model.DictData{},
		&model.Post{},
		&model.Notice{},
		&model.Config{},
		&model.LoginLog{},
	)
	ginx.Register("", func(r *gin.RouterGroup) {
		r.POST("/login", controller.LoginHandler)
		r.POST("/logout", controller.LogoutHandler)
		r.GET("/captcha", controller.CaptchaImageHandler)

	})
	ginx.Register("/api", func(r *gin.RouterGroup) {
		r.POST("/login", controller.ApiLoginHandler)
	})
	ginx.RegisterAuth("/system", func(r *gin.RouterGroup) {
		controller.Menu{}.Routers(r.Group("/menu"))
		controller.Role{}.Routers(r.Group("/role"))
		controller.User{}.Routers(r.Group("/user"))
		controller.Dept{}.Routers(r.Group("/dept"))
		controller.DictType{}.Routers(r.Group("/dict/type"))
		controller.Dict{}.Routers(r.Group("/dict/data"))
		controller.Post{}.Routers(r.Group("/post"))
		controller.Config{}.Routers(r.Group("/config"))
		controller.Notice{}.Routers(r.Group("/notice"))
		controller.Monitor{}.Routers(r.Group("/monitor"))
		controller.LoginLog{}.Routers(r.Group("/loginLog"))
	})
}

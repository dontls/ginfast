package internal

import (
	"fmt"
	"strconv"
	"time"

	"github.com/axgle/mahonia"
)

func Decimal2(v float64) float64 {
	value, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", v), 64)
	return value
}

var GStartTime time.Time = time.Now()

func RunTime() uint64 {
	return uint64(time.Since(GStartTime).Seconds())
}

var gbk = mahonia.NewDecoder("gbk")

func ToUTF8(b []byte) string {
	return gbk.ConvertString(string(b))
}

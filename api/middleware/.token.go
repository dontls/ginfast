package middleware

import (
	"bytes"
	"encoding/json"
	"ginfast/pkg/ginx"
	"ginfast/util"
	"io"

	"github.com/coocood/freecache"
	"github.com/gin-gonic/gin"
)

const (
	expiredTime = 30 * 60
)

var tokenStrore = freecache.NewCache(1024 * 1024)

// RemoveToken 删除token
func TokenRemove(token string) error {
	tokenStrore.Del([]byte(token))
	return nil
}

// CreateToken 生成Token
func TokenCreate(v *UserClaims) (string, error) {
	b, _ := json.Marshal(v)
	token := util.StringRandom(32)
	return token, tokenStrore.Set([]byte(token), b, expiredTime)
}

type tokenPara struct {
	Token string `json:"token" form:"token"`
}

func TokenExist(s string) error {
	_, err := tokenStrore.Get([]byte(s))
	return err
}

func UseTokenAuth(r *gin.RouterGroup) *gin.RouterGroup {
	r.Use(func(c *gin.Context) {
		b, _ := c.GetRawData()
		var p tokenPara
		c.ShouldBind(&p)
		val, err := tokenStrore.Get([]byte(p.Token))
		if err != nil {
			ginx.JSON(c).SetCode(ginx.StatusLoginExpired).SetMsg("invalid token").WriteData(nil)
			c.Abort()
			return
		}
		tokenStrore.Touch([]byte(p.Token), expiredTime)
		var u UserClaims
		if err := json.Unmarshal(val, &u); err == nil {
			c.Set("claims", u)
		}
		c.Request.Body = io.NopCloser(bytes.NewBuffer(b))
		c.Next()
	})
	return r
}

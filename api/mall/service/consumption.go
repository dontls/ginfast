package service

import (
	"errors"
	"ginfast/api/mall/model"
	"ginfast/pkg/orm"
)

// 会员消费更新信息
func UpdateConsume(p *model.ConsumptionOpt) error {
	var data model.User
	if err := orm.DbFirstByID(&data, p.UserID); err != nil {
		return err
	}

	if p.PayType == 1 {
		data.Balance += p.RechargeAmount
		if data.Balance < p.ConsumeAmount {
			return errors.New("insufficient balance, please recharge")
		}
		data.Balance -= p.ConsumeAmount
	}
	// 使用积分
	data.Integral += data.Integral
	return orm.DbUpdateFields(&data, "Balance", "Integral")
}

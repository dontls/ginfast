package dto

import (
	"ginfast/api/mall/model"
	"ginfast/pkg/orm"
)

type Where struct {
	*orm.DbPage
	OrderSn         string `form:"orderSn"`
	ReceiverKeyword string `form:"receiverKeyword"`
	OrderType       string `form:"orderType"`
	SourceType      string `form:"sourceType"` //
	Status          string `form:"status"`
	Phonenumber     string `form:"phonenumber"` // 手机号
	PayType         string `form:"payType"`     //
}

func (o *Where) DbModelWhere(v interface{}, handles ...func(dw *orm.DbWhere)) *orm.DbWhere {
	w := o.DbWhere()
	switch v.(type) {
	case *model.Order:
		if o.ReceiverKeyword != "" {
			w.Add("ReceiverName like ? or ReceiverPhone like ?", o.ReceiverKeyword, o.ReceiverKeyword)
		}
		w.Like("OrderSn", o.OrderSn)
		w.EqualNumber("SourceType", o.SourceType)
		w.EqualNumber("OrderType", o.OrderType)
		w.EqualNumber("Status", o.Status)
	case *model.User:
		w.Like("Phonenumber", o.Phonenumber)
	case *model.Consumption:
		w.EqualNumber("PayType", o.PayType)
	}
	for _, hle := range handles {
		hle(w)
	}
	return w.Model(v)
}

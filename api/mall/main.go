package mall

import (
	"ginfast/api/mall/controller"
	"ginfast/api/mall/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"

	"github.com/gin-gonic/gin"
)

// init 初始化
func init() {
	orm.RegisterModel(
		&model.User{},
		&model.UserAddress{},
		&model.Category{},
		&model.Goods{},
		&model.Order{},
		&model.Consumption{},
	)
	ginx.RegisterAuth("/mall", func(r *gin.RouterGroup) {
		controller.Category{}.Routers(r.Group("/category"))
		controller.Goods{}.Routers(r.Group("/goods"))
		controller.User{}.Routers(r.Group("/user"))
		controller.Brand{}.Routers(r.Group("/brand"))
		controller.UserAddress{}.Routers(r.Group("/user/addr"))
		controller.Consumption{}.Routers(r.Group("/consumption"))
		controller.Order{}.Routers(r.Group("/order"))
	})
}

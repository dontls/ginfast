package model

import "ginfast/pkg/orm"

// ConsumptionOpt 操作请求(获取/修改/更新)
type ConsumptionOpt struct {
	ID             uint    `json:"id" gorm:"primary_key"`
	UserID         uint    `json:"userId" gorm:"comment:会员号;"`
	UserName       string  `json:"userName" gorm:"comment:会员名称;"`
	GoodsName      string  `json:"goodsName" gorm:"comment:商品;"`
	PicUrl         string  `json:"picUrl" gorm:"comment:商品图片;"`
	Quantity       uint    `json:"quantity" gorm:"comment:商品数量;"`
	PayType        uint    `json:"payType" gorm:"default:0;type:varchar(1);comment:支付方式 0:现金 1:余额 2:微信 3:支付宝;"`
	RechargeAmount float64 `json:"rechargeAmount" gorm:"type:decimal(10,2);default:0;comment:充值金额;"`
	ConsumeAmount  float64 `json:"consumeAmount" gorm:"default:0;comment:消费金额;"`
	Integral       float64 `json:"integral" gorm:"type:decimal(10,2);default:0;comment:积分;"`
	Remark         string  `json:"remark" gorm:"comment:备注;"`
}

// Consumption 消费
type Consumption struct {
	ConsumptionOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Consumption) TableName() string {
	return "t_MallConsumption"
}

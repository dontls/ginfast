package model

import "ginfast/pkg/orm"

// CategoryOpt 操作请求(获取/修改/更新)
type CategoryOpt struct {
	ID          uint   `json:"id" gorm:"primary_key"`
	ParentID    uint   `json:"parentId" gorm:"comment:上机分类的编号：0表示一级分类;"`
	Name        string `json:"name"`
	NavStatus   string `json:"navStatus" gorm:"default:'0';type:varchar(1);comment:否显示在导航栏：0->不显示；1->显示;"`
	ShowStatus  string `json:"showStatus" gorm:"default:'0';type:varchar(1);comment:显示状态：0->不显示；1->显示;"`
	Sort        int    `json:"sort"`
	PicURL      string `json:"picUrl" gorm:"comment:图片;"`
	Description string `json:"description" gorm:"comment:描述;"`
}

// Category 分类
type Category struct {
	CategoryOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Category) TableName() string {
	return "t_MallCategory"
}

package model

import "ginfast/pkg/orm"

// UserOpt 操作请求(获取/修改/更新)
type UserOpt struct {
	ID          uint    `json:"id" gorm:"primary_key"`
	Name        string  `json:"name" gorm:"comment:姓名;"`
	Phonenumber string  `json:"phonenumber" gorm:"type:char(11);comment:手机号码;"`
	Status      string  `json:"status" gorm:"default:'0';type:varchar(1);comment:状态 0:正常 1:停用;"`
	Avatar      string  `json:"avatar" gorm:"comment:头像;"`
	Sex         string  `json:"sex" gorm:"default:'0';type:varchar(1);comment:性别 1:男 2:女 0:未知;"`
	Birthday    string  `json:"birthday" gorm:"comment:生日;"`
	SourceType  string  `json:"sourceType" gorm:"comment:用户来源;"`
	Balance     float64 `json:"balance" gorm:"type:decimal(10,2);default:0;comment:余额;"`
	Integral    float64 `json:"integral" gorm:"type:decimal(10,2);default:0;comment:积分;"`
	Remark      string  `json:"remark" gorm:"comment:备注;"`
}

// User 会员
type User struct {
	UserOpt
	orm.ModelTime
}

func (User) TableName() string {
	return "t_MallUser"
}

package model

import "ginfast/pkg/orm"

// // 订单详情
// // MallOrderAttrOpt 操作请求(获取/修改/更新)
// type MallOrderAttrOpt struct {
// 	ID           uint  `json:"id" gorm:"primary_key"`
// 	OrderNo      uint  `json:"orderNo" gorm:"comment:'订单单号';"`
// 	GoodsId      uint  `json:"goodsId" gorm:"comment:'商品Id';"`
// 	GoodsName    string  `json:"goodsName" gorm:"comment:'商品名';"`
// 	PicUrl       string  `json:"picUrl" gorm:"comment:'图片';"`
// 	Quantity     uint  `json:"quantity" gorm:"comment:'商品数量';"`
// 	SalesPrice   float64 `json:"salesPrice" gorm:"type:decimal(10,2);default:0;comment:购买单价;"`
// 	FreightPrice float64 `json:"freightPrice" gorm:"default:0;comment:'运费金额';"`
// 	PaymentPrice float64 `json:"paymentPrice" gorm:"default:0;comment:'支付金额（购买单价*商品数量+运费金额）';"`
// 	Remark       string  `json:"remark" gorm:"comment:'备注';"`
// }

// // MallOrderAttr 订单详情
// type MallOrderAttr struct {
// 	MallOrderAttrOpt
// 	TimeModel
// }

// 商品订单
// OrderOpt 操作请求(获取/修改/更新)
type OrderOpt struct {
	ID            uint    `json:"id" gorm:"primary_key;"`
	UserID        uint    `json:"userId" gorm:"comment:会员号;"`
	UserName      string  `json:"userName" gorm:"comment:会员名称;"`
	OrderNo       string  `json:"orderNo" gorm:"default:'';comment:'订单单号';"` // 关联订单详情
	Name          string  `json:"name" gorm:"default:'';type:varchar(64);comment:'订单名';"`
	IsPay         string  `json:"isPay" gorm:"default:'0';type:varchar(1);comment:'是否支付 0:未支付 1:已支付';"`
	PaymentWay    string  `json:"paymentWay" gorm:"default:'2';type:varchar(1);comment:'支付方式 1:货到付款  2:在线支付';"`
	Status        string  `json:"status" gorm:"default:'0';type:varchar(1);comment:'状态 0:待付款 1:待发货 2:待收货 3:已完成 4:已关闭';"`
	FreightPrice  float64 `json:"freightPrice" gorm:"default:0;comment:'运费金额';"`
	SalesPrice    float64 `json:"salesPrice" gorm:"type:decimal(10,2);default:0;comment:'销售金额';"`
	PaymentPrice  float64 `json:"paymentPrice" gorm:"type:decimal(10,2);default:0;comment:'支付金额:销售金额+运费金额-积分抵扣金额-电子券抵扣金额';"`
	Integration   uint    `json:"integration" gorm:"default:0;comment:获得的积分;"`
	UserMessage   string  `json:"userMessage" gorm:"comment:'买家留言';"`
	TransactionID string  `json:"transactionId" gorm:"comment:'支付交易ID';"`
	LogisticsID   string  `json:"logisticsId" gorm:"comment:'物流id';"`
}

// Order 订单
type Order struct {
	OrderOpt
	PaymentTime  orm.Time `json:"paymentTime" gorm:"comment:'付款时间';"`
	DeliveryTime orm.Time `json:"deliveryTime" gorm:"comment:'发货时间';"`
	ReceiverTime orm.Time `json:"receiverTime" gorm:"comment:'收货时间';"`
	ClosingTime  orm.Time `json:"closingTime" gorm:"comment:'成交时间';"`
}

func (Order) TableName() string {
	return "t_MallOrder"
}

package model

import "ginfast/pkg/orm"

// GoodsOpt 操作请求(获取/修改/更新)
type GoodsOpt struct {
	ID   uint   `json:"id" gorm:"primary_key"`
	Name string `json:"name"`
	// BrandId            uint   `json:"brandId"`
	// BrandName          string   `json:"brandName" gorm:"comment:'品牌名称';"`
	CategoryIds orm.UintArray `json:"categoryIds" gorm:"comment:商品分类数组;"`
	ProductSn   string        `json:"productSn" gorm:"comment:货号;"`
	Shelf       string        `json:"shelf" gorm:"default:'0';type:varchar(1);comment:架状态：0->下架；1->上架;"`
	// MewStatus          string   `json:"newStatus" gorm:"default:'0';type:varchar(1);comment:新品状态:0->不是新品；1->新品;"`
	// RecommandStatus    string   `json:"recommandStatus" gorm:"default:'0';type:varchar(1);comment:推荐状态；0->不推荐；1->推荐;"`
	// VerifyStatus       string   `json:"verifyStatus" gorm:"default:'0';type:varchar(1);comment:审核状态：0->未审核；1->审核通过;"`
	Sort           int      `json:"sort" gorm:"comment:排序;"`
	Sale           int      `json:"sale" gorm:"comment:销量;"`
	SalesPrice     *float64 `json:"salesPrice" gorm:"type:decimal(10,2);default:0;comment:价格"`
	OriginalPrice  *float64 `json:"originalPrice" gorm:"type:decimal(10,2);default:0;comment:市场价"`
	PromotionPrice *float64 `json:"promotionPrice" gorm:"type:decimal(10,2);default:0;comment:促销价格"`
	// GiftGrowth         int      `json:"giftGrowth" gorm:"default:0;comment:赠送的成长值"`
	GiftPoint     int `json:"giftPoint" gorm:"default:0;comment:赠送的积分"`
	UsePointLimit int `json:"usePointLimit" gorm:"default:0;comment:限制使用的积分数"`
	// SubTitle           string   `json:"subTitle" gorm:"comment:副标题;"`
	Stock int `json:"stock" gorm:"default:0;comment:库存"`
	// LowStock           int      `json:"lowStock" gorm:"default:0;comment:库存预警值"`
	// Unit               string   `json:"unit" gorm:"comment:单位;"`
	// Weight             float64  `json:"weight" gorm:"type:decimal(10,2);default:0;comment:商品重量，默认为克"`
	// PreviewStatus      string   `json:"previewStatus" gorm:"default:'0';type:varchar(1);comment:是否为预告商品：0->不是；1->是"`
	// ServiceIds         string   `json:"serviceIds" gorm:"comment:以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮;"`
	PicUrls orm.StringArray `json:"picUrls" gorm:"type:varchar(2048);comment:商品图片限制为5张,[]string to json"`
	// DetailTitle        string   `json:"detailTitle"`
	// PromotionStartTime jtime    `json:"promotionStartTime" gorm:"comment:'促销开始时间';"`
	// PromotionEndTime   jtime    `json:"promotionEndTime" gorm:"comment:'促销结束时间';"`
	// PromotionPerLimit  int      `json:"promotionPerLimit" gorm:"default:0;comment:活动限购数量"`
	// PromotionType      string   `json:"promotionType" gorm:"default:'0';type:varchar(1);comment:促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购"`
	Description string `json:"description" gorm:"comment:商品描述;"`
	// DetailDesc         string   `json:"detailDesc" gorm:"comment:'';"`
	// DetailHtml         string   `json:"detailHtml" gorm:"comment:'产品详情网页内容';"`
	// DetailMobileHtml   string   `json:"detailMobileHtml" gorm:"comment:'移动端网页详情';"`
}

// MallGoods 商品
type Goods struct {
	GoodsOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Goods) TableName() string {
	return "t_MallGoods"
}

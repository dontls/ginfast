package model

import "ginfast/pkg/orm"

// UserAddressOpt 操作请求(获取/修改/更新)
type UserAddressOpt struct {
	ID           uint   `json:"id" gorm:"primary_key"`
	UserID       string `json:"userId" gorm:"comment:会员编号;"`
	RecvName     string `json:"recvname" gorm:"comment:收货人名字;"`
	TelNum       string `json:"telNum" gorm:"comment:收货人电话;"`
	PostalCode   string `json:"postalCode" gorm:"comment:收货人邮编;"`
	ProvinceName string `json:"provinceName" gorm:"comment:省份/直辖市;"`
	CityName     string `json:"cityName" gorm:"comment:城市;"`
	CountyName   string `json:"countyName" gorm:"comment:区;"`
	DetailInfo   string `json:"detailInfo" gorm:"comment:详细地址;"`
	IsDefault    string `json:"isDefault" gorm:"default:'0';type:varchar(1);comment:是否默认 1是0否;"`
}

// UserAddress 会员地址
type UserAddress struct {
	UserAddressOpt
	orm.ModelTime
}

func (UserAddress) TableName() string {
	return "t_MallUserAddress"
}

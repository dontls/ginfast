package model

import "ginfast/pkg/orm"

// BrandOpt 操作请求(获取/修改/更新)
type BrandOpt struct {
	ID                  uint   `json:"id" gorm:"primary_key"`
	Name                string `json:"name"`
	FirstLetter         string `json:"firstLetter" gorm:"comment:首字母;"`
	Sort                int    `json:"sort"`
	FactoryStatus       string `json:"factoryStatus" gorm:"default:'0';type:varchar(1);comment:是否为品牌制造商：0->不是；1->是;"`
	ShowStatus          string `json:"showStatus" gorm:"default:'0';type:varchar(1);comment:显示状态：0->不显示；1->显示;"`
	ProductCount        int    `json:"productCount"`
	ProductCommentCount int    `json:"productCommentCount" gorm:"comment:产品评论数量;"`
	Logo                string `json:"logo" gorm:"comment:品牌logo;"`
	BigPic              string `json:"bigPic" gorm:"comment:专区大图;"`
	BrandStory          string `json:"brandStory" gorm:"comment:品牌故事;"`
}

// Brand 品牌
type Brand struct {
	BrandOpt
	orm.ModelAuth
	orm.ModelTime
}

func (Brand) TableName() string {
	return "t_MallBrand"
}

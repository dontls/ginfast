package controller

import (
	"ginfast/api/mall/dto"
	"ginfast/api/mall/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// UserAddress 地址
type UserAddress struct {
}

// ListHandler 列表
func (UserAddress) ListHandler(c *gin.Context) {
	var data []model.UserAddress
	err := orm.DbFind(&data)
	ginx.JSON(c).WriteData(data, err)
}

// PageHandler 分页
func (UserAddress) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.UserAddress
	total, _ := p.DbModelWhere(&model.UserAddress{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (UserAddress) GetHandler(c *gin.Context) {
	util.QueryByID(&model.UserAddress{}, ginx.JSON(c))
}

// AddHandler 新增
func (UserAddress) AddHandler(c *gin.Context) {
	var p model.UserAddress
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserAddressOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (UserAddress) UpdateHandler(c *gin.Context) {
	var p model.UserAddress
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserAddressOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (UserAddress) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.UserAddress{}, ginx.JSON(c))
}

// UpdateStatusHandler 修改
func (UserAddress) UpdateDefaultHandler(c *gin.Context) {
	var p model.UserAddress
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserAddressOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateFields(&p, "IsDefault")
	ctx.WriteError(err)
}

func (o UserAddress) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/page", o.PageHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.PUT("/default", o.UpdateDefaultHandler)
}

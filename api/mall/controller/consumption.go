package controller

import (
	"ginfast/api/mall/dto"
	"ginfast/api/mall/model"
	"ginfast/api/mall/service"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Consumption 分类
type Consumption struct {
}

// PageHandler 分页
func (Consumption) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Consumption
	total, _ := p.DbModelWhere(&model.Consumption{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (Consumption) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Consumption{}, ginx.JSON(c))
}

// AddHandler 新增
func (Consumption) AddHandler(c *gin.Context) {
	var p model.Consumption
	//获取参数
	ctx, err := ginx.MustBind(c, &p.ConsumptionOpt)
	if err != nil {
		return
	}
	if err := service.UpdateConsume(&p.ConsumptionOpt); err != nil {
		ctx.WriteError(err)
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Consumption) UpdateHandler(c *gin.Context) {
	var p model.Consumption
	//获取参数
	ctx, err := ginx.MustBind(c, &p.ConsumptionOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

func (o Consumption) Routers(r *gin.RouterGroup) {
	r.GET("/page", o.PageHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
}

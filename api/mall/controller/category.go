package controller

import (
	"ginfast/api/mall/dto"
	"ginfast/api/mall/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/pkg/tree"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Category 分类
type Category struct {
}

// ListHandler 列表
func (Category) ListHandler(c *gin.Context) {
	var data []model.Category
	err := orm.DbFind(&data)
	ginx.JSON(c).WriteData(data, err)
}

// PageHandler 列表
func (Category) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Category
	total, _ := p.DbModelWhere(&model.Category{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// TreeHandler 列表
func (Category) TreeHandler(c *gin.Context) {
	var trees []tree.Node
	orm.DB().Model(model.Category{}).Scan(&trees)
	data := tree.Build(trees)
	ginx.JSON(c).WriteData(data)
}

// ListHandler 列表
func (Category) ListParentHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	id := c.Param("id")
	var data []model.Category
	total, _ := p.DbModelWhere(&model.Category{}, func(dw *orm.DbWhere) {
		dw.EqualNumber("ParentID", id)
	}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (Category) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Category{}, ginx.JSON(c))
}

// AddHandler 新增
func (Category) AddHandler(c *gin.Context) {
	var p model.Category
	//获取参数
	ctx, err := ginx.MustBind(c, &p.CategoryOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (Category) UpdateHandler(c *gin.Context) {
	var p model.Category
	//获取参数
	ctx, err := ginx.MustBind(c, &p.CategoryOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Category) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Category{}, ginx.JSON(c))
}

func (o Category) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/list/:id", o.ListParentHandler)
	r.GET("/tree", o.TreeHandler)
	r.GET("/page", o.PageHandler)
	r.GET("/:id", o.GetHandler)
	r.PUT("", o.UpdateHandler)
	r.POST("", o.AddHandler)
	r.DELETE("", o.DeleteHandler)
}

package controller

import (
	"ginfast/api/mall/dto"
	"ginfast/api/mall/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// User 分类
type User struct {
}

// ListHandler 列表
func (User) ListHandler(c *gin.Context) {
	var data []model.User
	err := orm.DbFind(&data)
	ginx.JSON(c).WriteData(data, err)
}

// PageHandler 分页
func (User) PageHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.User
	total, _ := p.DbModelWhere(&model.User{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (User) GetHandler(c *gin.Context) {
	util.QueryByID(&model.User{}, ginx.JSON(c))
}

// AddHandler 新增
func (User) AddHandler(c *gin.Context) {
	var p model.User
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 修改
func (User) UpdateHandler(c *gin.Context) {
	var p model.User
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// UpdateStatusHandler 修改
func (User) UpdateStatusHandler(c *gin.Context) {
	var p model.User
	//获取参数
	ctx, err := ginx.MustBind(c, &p.UserOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateFields(&p, "Status")
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (User) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.User{}, ginx.JSON(c))
}

func (o User) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/page", o.PageHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.PUT("/status", o.UpdateStatusHandler)
}

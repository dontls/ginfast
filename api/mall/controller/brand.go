package controller

import (
	"ginfast/api/mall/dto"
	"ginfast/api/mall/model"
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
	"ginfast/util"

	"github.com/gin-gonic/gin"
)

// Brand 品牌
type Brand struct {
}

// ListHandler 列表
func (Brand) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.Brand
	total, _ := p.DbModelWhere(&model.Brand{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 查询
func (Brand) GetHandler(c *gin.Context) {
	util.QueryByID(&model.Brand{}, ginx.JSON(c))
}

// AddHandler 新增
func (Brand) AddHandler(c *gin.Context) {
	var p model.Brand
	//获取参数
	ctx, err := ginx.MustBind(c, &p.BrandOpt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p)
	ctx.WriteError(err)
}

// UpdateHandler 更新
func (Brand) UpdateHandler(c *gin.Context) {
	var p model.Brand
	//获取参数
	ctx, err := ginx.MustBind(c, &p.BrandOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

type showStatus struct {
	IDS        string `form:"ids"`
	ShowStatus string `form:"showStatus"`
}

func (Brand) UpdateShowHandler(c *gin.Context) {
	var p showStatus
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	ids := util.StringSplit(p.IDS, ",")
	err = orm.DbUpdateByID(&model.Brand{}, ids, orm.H{"ShowStatus": p.ShowStatus})
	ctx.WriteError(err)
}

// UpdateFactoryHandler 更新
func (Brand) UpdateFactoryHandler(c *gin.Context) {
	var p model.Brand
	//获取参数
	ctx, err := ginx.MustBind(c, &p.BrandOpt)
	if err != nil {
		return
	}
	err = orm.DbUpdateFields(&p, "FactoryStatus")
	ctx.WriteError(err)
}

// DeleteHandler 删除
func (Brand) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.Brand{}, ginx.JSON(c))
}

func (o Brand) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.POST("/showStatus", o.UpdateShowHandler)
	r.POST("/factoryStatus", o.UpdateFactoryHandler)
	r.DELETE("", o.DeleteHandler)
}

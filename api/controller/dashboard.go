package controller

import (
	"ginfast/pkg/ginx"

	"github.com/gin-gonic/gin"
)

// Dashboard 面板
type Dashboard struct {
}

type groupData struct {
	TotalMembers int     `json:"totalMembers"`
	TotalPoints  float64 `json:"totalPoints"`
	SalesVolume  float64 `json:"salesVolume"`
	TotalOrders  int     `json:"totalOrders"`
}

// GroupHandler 列表
func (Dashboard) GroupHandler(c *gin.Context) {
	var data groupData
	ginx.JSON(c).WriteData(data)
}

func (o Dashboard) Routers(r *gin.RouterGroup) {
	r.GET("/group", o.GroupHandler)
}

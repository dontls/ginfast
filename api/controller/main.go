package controller

import (
	"ginfast/pkg/ginx"

	"github.com/gin-gonic/gin"
)

func init() {
	ginx.Register("/", func(r *gin.RouterGroup) {
		DbInit{}.Routers(r.Group("/dbInit"))
	})
	ginx.RegisterAuth("/", func(r *gin.RouterGroup) {
		r.POST("/upload", UploadPicHandler)
		Dashboard{}.Routers(r.Group("/dashboard"))
	})
}

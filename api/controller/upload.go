package controller

import (
	"ginfast/configs"
	"ginfast/pkg/ginx"
	"os"

	"github.com/gin-gonic/gin"
)

// UploadPicHandler 图片服务
func UploadPicHandler(c *gin.Context) {
	filename := ""
	fileHead, err := c.FormFile("file")
	if err == nil {
		filename = configs.Public.Abs("upload", "picture", fileHead.Filename)
		os.MkdirAll(filename, os.ModePerm)
		err = c.SaveUploadedFile(fileHead, filename)
	}
	ginx.JSON(c).WriteData(gin.H{"name": filename, "url": filename}, err)
}

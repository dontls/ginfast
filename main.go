package main

import (
	"fmt"
	"ginfast/api"
	"ginfast/configs"
	"ginfast/initialize"
	"ginfast/pkg/svc"
	"log"
	"os"
)

// http://patorjk.com/software/taag/#p=display&f=Slant&t=ginfast
const logo = `
 ██████╗ ██╗███╗   ██╗███████╗ █████╗ ███████╗████████╗
██╔════╝ ██║████╗  ██║██╔════╝██╔══██╗██╔════╝╚══██╔══╝
██║  ███╗██║██╔██╗ ██║█████╗  ███████║███████╗   ██║   
██║   ██║██║██║╚██╗██║██╔══╝  ██╔══██║╚════██║   ██║   
╚██████╔╝██║██║ ╚████║██║     ██║  ██║███████║   ██║   
 ╚═════╝ ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚══════╝   ╚═╝   
`

// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/
func main() {
	fmt.Printf("%s\n", logo)
	if err := configs.Load("config.yaml"); err != nil {
		log.Fatal(err)
	}
	svc.Run(&svc.Program{
		AppName: func() string {
			if len(os.Args) > 2 {
				configs.AppName = os.Args[2]
			}
			return configs.AppName
		},
		Description: configs.AppName + " service application",
		Run: func() error {
			if err := initialize.Run(); err != nil {
				return err
			}
			return api.Run()
		},
		Shutdown: func() error {
			return api.Shutdown()
		},
	})
}

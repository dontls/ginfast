package initialize

import (
	"errors"
	"ginfast/configs"
	"ginfast/pkg/orm"

	"github.com/glebarez/sqlite"
	"gorm.io/driver/mysql"
)

type DbOptions struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

func Database() error {
	var o DbOptions
	if err := configs.GViper.UnmarshalKey("sql", &o); err != nil {
		return err
	}
	// orm.SetLogger(log.Log())
	switch o.Name {
	case "mysql":
		// root:123456@tcp(localhost:3306)/ginfast?charset=utf8mb4&parseTime=True&loc=Local
		return orm.CreateDB(mysql.New(mysql.Config{
			DSN: o.Address,
			// DefaultStringSize:         64,    // string 类型字段的默认长度
			DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
			DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
			DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
			SkipInitializeWithVersion: false, // 根据版本自动配置
		}), false)
	case "sqlite":
		return orm.CreateDB(sqlite.Open(o.Address), false)
	}
	return errors.New("invalid database")
}

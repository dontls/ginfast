package initialize

import (
	"ginfast/configs"
	"ginfast/pkg/log"
)

func Logger() error {
	var logcfg log.Logger
	if err := configs.GViper.UnmarshalKey("log", &logcfg); err != nil {
		return err
	}
	log.Output(&logcfg, configs.GViper.GetInt("log.level"))
	return nil
}

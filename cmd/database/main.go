package main

import (
	"ginfast/api/system/model"
	"ginfast/configs"
	"ginfast/initialize"
	"ginfast/pkg/orm"
	"ginfast/util"
	"log"
)

func main() {
	if err := configs.Load("../../config.yaml"); err != nil {
		log.Fatalln(err)
	}
	orm.RegisterModel(
		&model.Menu{},
		&model.DictType{},
		&model.DictData{},
	)
	log.Println("connect database.", initialize.Database())
	if orm.DbCount(&model.Menu{}, nil) < 1 {
		var data []model.Menu
		util.JSONFile("menu.json", &data)
		orm.DbCreate(&data)
	}
	if orm.DbCount(&model.DictType{}, nil) < 1 {
		var data []model.DictType
		util.JSONFile("dicttype.json", &data)
		orm.DbCreate(&data)
	}
	if orm.DbCount(&model.DictData{}, nil) < 1 {
		var data []model.DictData
		util.JSONFile("dictdata.json", &data)
		orm.DbCreate(&data)
	}
}

package util

import (
	"ginfast/pkg/ginx"
	"ginfast/pkg/orm"
)

func Deletes(v interface{}, c *ginx.Context) {
	idstr, err := c.Param("id"), error(nil)
	if idstr != "" {
		ids := StringSplit(idstr, ",")
		err = orm.DbDeleteByIDs(v, ids)
	}
	c.WriteError(err)
}

func QueryByID(v interface{}, c *ginx.Context) {
	_, queryId := c.ParamUInt("id")
	err := orm.DbFirstByID(v, queryId)
	c.WriteData(v, err)
}

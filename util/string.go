package util

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

// StringRandom 生成指定位数的字符
func StringRandom(width int) string {
	// var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	// b := make([]rune, width)
	// for i := range b {
	// 	b[i] = letterRunes[rand.New(rand.NewSource(time.Now().UnixNano())).Intn(len(letterRunes))]
	// }
	// return string(b)
	randBytes := make([]byte, width/2)
	rand.Read(randBytes)
	return fmt.Sprintf("%x", randBytes)

}

func StringSplit(str, sep string) []int {
	strv := strings.Split(str, sep)
	var intv []int
	for _, v := range strv {
		if v == "" {
			continue
		}
		val, _ := strconv.Atoi(v)
		intv = append(intv, val)
	}
	return intv
}

func StringVal(s string, v any) error {
	return json.Unmarshal([]byte(s), v)
}

func JString(v interface{}) string {
	if val, err := json.Marshal(v); err == nil {
		return string(val)
	}
	return ""
}

package {{.App}}

import (
	"ginfast/api/{{.App}}/controller"
	"ginfast/api/{{.App}}/model"
	"ginfast/pkg/orm"
	"ginfast/pkg/ginx"

	"github.com/gin-gonic/gin"
)

// init 初始化
func init() {
	orm.RegisterModel(
		&model.{{.Model}}{},
	)
	ginx.RegisterAuth("/{{.App}}", func(r *gin.RouterGroup) {
		controller.{{.Model}}{}.Routers(r.Group("/{{.Name}}"))
	})
}

package dto

import (
	"ginfast/api/{{.App}}/model"
	"ginfast/pkg/orm"
)

type Where struct {
	*orm.DbPage
}

func (o *Where) DbModelWhere(v interface{}, handles ...func(dw *orm.DbWhere)) *orm.DbWhere {
	w := o.DbWhere()
	switch v.(type) {
	case *model.{{.Model}}:
	}
	for _, h := range handles {
		h(w)
	}
	return w.Model(v)
}

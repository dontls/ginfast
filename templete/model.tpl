package model

// {{.Model}}Opt
type {{.Model}}Opt struct {
	ID uint `json:"id" gorm:"primary_key"`
}

// {{.Model}}
type {{.Model}} struct {
	{{.Model}}Opt
}

func ({{.Model}}) TableName() string {
	return "t_{{.App}}{{.Model}}"
}

package controller

import (
	"ginfast/api/{{.App}}/dto"
	"ginfast/api/{{.App}}/model"
	"ginfast/pkg/orm"
	"ginfast/pkg/ginx"
	"ginfast/util"
	
	"github.com/gin-gonic/gin"
)

// {{.Model}}
type {{.Model}} struct {
}

// ListHandler 列表
func ({{.Model}}) ListHandler(c *gin.Context) {
	var p dto.Where
	ctx, err := ginx.MustBind(c, &p)
	if err != nil {
		return
	}
	var data []model.{{.Model}}
	total, _ := p.DbModelWhere(&model.{{.Model}}{}).Find(&data)
	ctx.WriteTotal(total, data)
}

// GetHandler 获取指定id
func ({{.Model}}) GetHandler(c *gin.Context) {
	util.QueryByID(&model.{{.Model}}{}, ginx.JSON(c))
}

// AddHandler 新增
func ({{.Model}}) AddHandler(c *gin.Context) {
	var p model.{{.Model}}
	//获取参数
	ctx, err := ginx.MustBind(c, &p.{{.Model}}Opt)
	if err != nil {
		return
	}
	err = orm.DbCreate(&p);
	ctx.WriteError(err)
}

// UpdateHandler 修改
func ({{.Model}}) UpdateHandler(c *gin.Context) {
	var p model.{{.Model}}
	//获取参数
	ctx, err := ginx.MustBind(c, &p.{{.Model}}Opt)
	if err != nil {
		return
	}
	err = orm.DbUpdateModel(&p)
	ctx.WriteError(err)
}

// DeleteHandler 删除
func ({{.Model}}) DeleteHandler(c *gin.Context) {
	util.Deletes(&model.{{.Model}}{}, ginx.JSON(c))
}

func(o {{.Model}}) Routers(r *gin.RouterGroup) {
	r.GET("/list", o.ListHandler)
	r.GET("/:id", o.GetHandler)
	r.POST("", o.AddHandler)
	r.PUT("", o.UpdateHandler)
	r.DELETE("/:id", o.DeleteHandler)
}
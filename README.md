# ginfast
```shell

 ██████╗ ██╗███╗   ██╗███████╗ █████╗ ███████╗████████╗
██╔════╝ ██║████╗  ██║██╔════╝██╔══██╗██╔════╝╚══██╔══╝
██║  ███╗██║██╔██╗ ██║█████╗  ███████║███████╗   ██║   
██║   ██║██║██║╚██╗██║██╔══╝  ██╔══██║╚════██║   ██║   
╚██████╔╝██║██║ ╚████║██║     ██║  ██║███████║   ██║   
 ╚═════╝ ╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚══════╝   ╚═╝   
                                                       
                 
```

## 使用说明
   
### pkg
 - [gitee](https://gitee.com/dontls/pkg)
 - [github](https://github.com/dontls/pkg)

## embedfs编译
 - go build -tags="embfs"

## 功能列表
 - 服务注册
 - [代码生成](https://gitee.com/dontls/ginfast/blob/master/cmd/generator/main.go)
 - [插件路由](https://gitee.com/dontls/ginfast/blob/master/templete/main.tpl)
 - [更多功能](https://gitee.com/dontls/pkg)

## 例子
 - [jtvss](https://gitee.com/jtvs/jtvss)

## 特别感谢❤ 
